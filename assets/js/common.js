var latitude, longitude, user_city, primaryCat, currentPageIndex = 0;
$(document).ready(function(){
	
	$.ajax({
	  "async": true,
		"url":'http://api.ipstack.com/132.147.68.33?access_key=5024bad5e45ffc84d8cc34685e991807',
		"method": "GET"
	}).done(function(pc_data){
		
		latitude = pc_data.latitude.toFixed(2);
    longitude = pc_data.longitude.toFixed(2);
    user_city = pc_data.city;
    getVenueByCategory();
	});

	getVenueCategory();

	$('input[type="checkbox"]').on('change',function(){
		currentPageIndex = 0;
		getVenueByCategory();
	});
	
		 
	
});


function getVenueCategory() {
	// body...,

	var settings = {
		"async": true,
		"url": "https://api.foursquare.com/v2/venues/categories?client_id=2NBMKEZ2YYUQSTVMZ5ZD04DXNCCV5YR3JXGCJ4BFGOMJXZRG&client_secret=GSLQ1Q5D4IZB2GJHO150KJOP4IDNXHCOKNYL3DUKJIIA4CJJ&v=20180323",
		"method": "GET"
	};

	$.ajax(settings).done(function (cat_data) {
		primaryCat = cat_data.response.categories;
		$.each(primaryCat, function(i, pCat_item){
			$('#primaryCatSelect').append("<option value="+pCat_item.id+">"+pCat_item.name+"</option>")

		});
		selectPrimaryCategory();
	});
}

function selectPrimaryCategory(){
	$('#primaryCatSelect').on('change',function(){
		$('#subCatSelect').prop('disabled', false);
		currentPageIndex = 0;
		var selectedSubCat = findSubCategory(primaryCat, "id", $(this).val());
		$('#subCatSelect').html("<option default selected value=''>Select venue type (Subcategory)</option>");
		if(selectedSubCat.categories.length > 0) {
			$.each(selectedSubCat.categories, function(i,subCat_item){
				$('#subCatSelect').append("<option value="+subCat_item.id+">"+subCat_item.name+"</option>")
			});
			selectSecondaryCategory();
		}
		else {
			$('#subCatSelect').prop('disabled', true);
		}
		
		getVenueByCategory();
		
	});
}

function selectSecondaryCategory(){
	$('#subCatSelect').on('change',function(){
		currentPageIndex = 0;
		getVenueByCategory();
	});
}

function findSubCategory(arr, propName, propValue) {
  for (var i=0; i < arr.length; i++) {
    if (arr[i][propName] == propValue) {
      return arr[i] ;
    }
  }
}

function getVenueViaLatLong(lat,long){
	var settings = {
	  "async": true,
	  // "url": "/assets/js/venue-data.json",
	  "url": "http://api.foursquare.com/v2/venues/explore?client_id=2NBMKEZ2YYUQSTVMZ5ZD04DXNCCV5YR3JXGCJ4BFGOMJXZRG&client_secret=GSLQ1Q5D4IZB2GJHO150KJOP4IDNXHCOKNYL3DUKJIIA4CJJ&v=20180323&limit=15&ll="+latitude+","+longitude,
	  "method": "GET"
	};

	$.ajax(settings).done(function (v_data) {
	  var venues_data= new Array();
	  var count = 0;
	  var tableContent = "";
	  venues_data=v_data.response.groups[0].items;
	  var totalCount = v_data.response.totalResults;
	  createPagination(totalCount);
	  $('.page-title .data-title').html(v_data.response.groups[0].type + " of "+ user_city);

	  $.each(venues_data, function(i, venue_item) {
	  	count++;
			var venueName = venue_item.venue.name;
			var venueAddress = venue_item.venue.location.address;
			var venueCategory;
	    if (venue_item.venue.categories.length >  1) {
	    	$.each(venue_item.venue.categories, function(j, category_item){
		    	venueCategory += category_item.name;
		    	if (j < venue_item.venue.categories.length) {
		    		venueCategory += " , ";
		    	}
		    });
	    }
	    else {
	    	venueCategory = venue_item.venue.categories[0].name;
	    }

	    tableContent += "<tr> <th>"+count+"</th> <td>"+venueName+"</td> <td>"+venueAddress+"</td> <td>"+venueCategory+"</td> </tr>";
	    
	  });
  	$('#venueList tbody').html(tableContent)
	});


	  
}

function getVenueByCategory() {

	var url_name = "http://api.foursquare.com/v2/venues/explore?client_id=2NBMKEZ2YYUQSTVMZ5ZD04DXNCCV5YR3JXGCJ4BFGOMJXZRG&client_secret=GSLQ1Q5D4IZB2GJHO150KJOP4IDNXHCOKNYL3DUKJIIA4CJJ&v=20180323&limit=15";
	if($('#openingStatus').prop('checked')) {
		url_name += "&openNow=1";
	}

	if($('#popularity').prop('checked')) {
		url_name += "&sortByPopularity=1";
	}

	if(currentPageIndex > 1) {
		var offsetNum = (currentPageIndex - 1) * 15;
		url_name += "&offset=" + offsetNum;
	}
	if ($('#subCatSelect').val() != '') {
		url_name += "&categoryId=" + $('#subCatSelect').val();
	}
	else if($('#primaryCatSelect').val() != '') {
		url_name += "&categoryId=" + $('#primaryCatSelect').val();
		
	}
	
	var settings = {
	  "async": true,
	  // "url": "/assets/js/venue-data.json",
	  "url": url_name+"&ll="+latitude+","+longitude,
	  "method": "GET"
	};
	$.ajax(settings).done(function (v_data) {
	  var venues_data= new Array();
	  if(currentPageIndex > 1) {
	  	var count = (currentPageIndex - 1) * 15;
	  }
	  else {
			var count = 0;
	  }
	  
	  venues_data=v_data.response.groups[0].items;
	  var totalCount = v_data.response.totalResults;
	  createPagination(totalCount);
	  $('.page-title .data-title').html(v_data.response.groups[0].type + " of "+ user_city);
	  var tableContent ="";
	  $.each(venues_data, function(i, venue_item) {
	  	count++;
			var venueName = venue_item.venue.name;

			var venueAddress = venue_item.venue.location.address;
			var venueCategory;
	    if (venue_item.venue.categories.length >  1) {
	    	$.each(venue_item.venue.categories, function(j, category_item){
		    	venueCategory += category_item.name;
		    	if (j < venue_item.venue.categories.length) {
		    		venueCategory += " , ";
		    	}
		    });
	    }
	    else {
	    	venueCategory = venue_item.venue.categories[0].name;
	    }

	    tableContent += "<tr> <th>"+count+"</th> <td>"+venueName+"</td> <td>"+venueAddress+"</td> <td>"+venueCategory+"</td> </tr>";
	    
	  	
	  });
	  $('#venueList tbody').html(tableContent);
	});
	  
}

function createPagination(totalCount){
	console.log(totalCount);
	if(totalCount < 16) {
		$('.pagination-gp').hide();
	}
	else {
		$('.pagination-gp').show();
		var paginationContent="";
		var pageCount = Math.ceil(totalCount / 15);
		for(var i = 1; i <= pageCount; i++) {
	    paginationContent += "<li class='page-item'><a class='page-link' href='#' data-pageId='"+i+"''>"+i+"</a></li>";
		}
		console.log(pageCount);
		$('.pagination').html(paginationContent);
		goToPageIndex();
	}
	

}

function goToPageIndex(){
	$('.pagination li a').on('click',function(){
		currentPageIndex = parseInt($(this).attr('data-pageId'));
		getVenueByCategory();
	});
}
